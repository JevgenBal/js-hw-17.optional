"use strict"

let student = {
  name: "",
  lastName: "",
  table: {}
};

student.name = prompt("Введіть ім'я студента:");
student.lastName = prompt("Введіть прізвище студента:");

while (true) {
  let subject = prompt("Введіть назву предмета:");

  if (!subject) {
  break;
  }

  let grade = parseFloat(prompt(`Введіть оцінку за ${subject}:`));
  student.table[subject] = grade;
}

let numberOfFails = 0;
let sumOfGrades = 0;
let numberOfGrades = 0;

for (let subject in student.table) {
  if (student.table[subject] < 4) {
  numberOfFails++;
  }

  sumOfGrades += student.table[subject];
  numberOfGrades++;
}

let averageGrade = sumOfGrades / numberOfGrades;

if (numberOfFails > 0) {
  console.log(`Студент не зарахований. Кількість поганих оцінок: ${numberOfFails}`);
  }
  else {
  console.log("Студента переведено на наступний курс!");
  }

  if (averageGrade > 7) {
  console.log("Студенту призначено стипендію!!!");
};